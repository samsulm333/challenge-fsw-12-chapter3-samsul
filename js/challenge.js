var logo = document.getElementById("logo");
logo.addEventListener("mouseover", function() {
    logo.style.color = "red"
}, false);
logo.addEventListener("mouseout", function() {
    logo.style.color = "white"
}, false);

var twitch = document.getElementById("twitch");
twitch.addEventListener("mouseover", function() {
    twitch.style.fill = "purple"
}, false);
twitch.addEventListener("mouseout", function() {
    twitch.style.fill = "white"
}, false);